# Цифровой генератор сигналов LESO5 #


C/C++ интерфейс для управление прибором
Выполнен в виде динамической подключаемой библиотеке
Поддерживает операционные системы: Windows, Linux

#Список изменений в v02.01.xx  
* Изменил интерфейс `leso5GetPortDevice`  
* Имя COM порта изменил на "\\\\.\\COM%d". Так можно открывать порты больше чем COM9
* В функцию `leso5UpdateDeviceList` добавил параметр -- максимальный номер порта для сканирования.  
* Изменил интерфейс `leso5GetDeviceDescriptor`  
* В функциях `leso5SetReg16` и `leso5SetReg32` сменил тип адреса.  
* Добавил функцию, возвращающую версию драйвера(библиотеки) `leso5GetDriverVersion`
* Обновил пример программы.