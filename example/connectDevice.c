
// gcc -o connectDevice connectDevice.c -I../ -lLESO5 -L../

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <leso5.h>

#define FREQ 500000 // 500 KHz
#define DAC_FREQ 125000000 // 125 MHz
#define DIV 4294967296

#define MAX_PORT_NAME 		(200)
#define MAX_PORT_NAME_LEN	(20)
#define MAX_DESCRIPTOR_LEN	(20)

int main()
{
	char devicePort[MAX_PORT_NAME_LEN];
	char descriptor[MAX_DESCRIPTOR_LEN];
	int numberDevice;
	uint32_t driverVersion;

	if (leso5GetDriverVersion(&driverVersion) == LESO5_ERROR) {
		fprintf(stderr,"leso5GetDriverVersion: error \n");
		return -1;
	}
	printf("Driver Version: %d.%d.%d\n",(driverVersion>>16) & 0xFF,
										(driverVersion>>8) & 0xFF,
										driverVersion & 0xFF);


	numberDevice = leso5UpdateDeviceList(MAX_PORT_NAME);


	if (leso5GetPortDevice(0, devicePort,  sizeof(devicePort))) {
		fprintf(stderr,"leso5GetPortDevice: error\r\n");
		return -1;
	} else	printf("Found %d device (%s)\n", numberDevice, devicePort);

	if (leso5Open(devicePort) == LESO5_ERROR) {
		fprintf(stderr,"Error open device - %s\n", devicePort);
		return -1;
	}

	if (leso5GetDeviceDescriptor(descriptor, sizeof(descriptor)) == LESO5_ERROR) {
		fprintf(stderr,"leso5GetDeviceDescriptor: error\n");
	} else
		printf("Device Descriptor: %s\n", descriptor);



//	leso5SetReg16(ADDR_NCO	| CH_A, 0x01);		// Select SAW Signal
	leso5SetReg16(ADDR_GAIN | CH_A, 0xFFFF);	// Max Gain
	leso5SetReg16(ADDR_MUX	| CH_A, 0x01);		// Select NCO
	leso5SetReg32(ADDR_FACC | CH_A, FREQ*DIV/DAC_FREQ); // Set Signal frequency = FREQ

	return 0;
}
