
#ifndef LESO5Device_EXPORT_H
#define LESO5Device_EXPORT_H

#include "UARTDevice.h"

#include <cstdio>
#include <iostream>
#include <vector> 
#include <string>

using std::cout;
using std::vector;
using std::string;

#define DEVICE_BAUD_RATE 115200
#define MAX_DEVICE_NAME_LENGTH 20
#define READ_TIMEOUT_MS 100
#define MAX_PORT_NAME_LENGTH 20

#ifdef _WINDOWS
#define COM_PORTS_NAME "\\\\.\\COM%d"
#else
#define COM_PORTS_NAME "/dev/ttyUSB%d"
#endif

class LESO5Device
{
	UARTDevice serial;
	vector<string> leso5DeviceLst;
	string currentOpenDeviceName;
	
public:
	LESO5Device();
	~LESO5Device();
	
	int open(const char *serialPort);
	void close();
	void deviceScan(int potNum = 10);
	bool isLESO5Device();
	void switchLed(bool);
	bool sendCmd(char *cmd, int cmd_length);
	
	string getDeviceName() { return currentOpenDeviceName; }
	vector<string> *getDeviceLst() { return &leso5DeviceLst; }
};

#endif
