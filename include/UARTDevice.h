#ifndef UART_DEVICE_H
#define UART_DEVICE_H

#include <serialib.h>

class UARTDevice : public serialib
{
	bool openFlag;
	
public:
	UARTDevice();
	~UARTDevice();
	
	char Open(const char *Device, const unsigned int Bauds); 	
	bool isOpen() { return openFlag; }
};

#endif