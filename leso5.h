/**
\file
\author Ryasanov Ilya <ryasanov@gmail.com> www.labfor.ru
\author Shauerman Alexander <shamrel@yandex.ru> www.labfor.ru
\brief Библиотека предоставляющая API для взаимодействия с генератором сигналов LESO5
\details
\date 31.03.2016
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#ifndef LESO5_EXPORT_H
#define LESO5_EXPORT_H

#include <stdint.h>

#ifdef _WINDOWS
#define LESO_DLL __declspec(dllexport)
#else
#define LESO_DLL
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define VERSION_MAJOR	(02)
#define VERSION_MINOR	(01)
#define VERSION_BUILD	(12)

/*!
\defgroup address_reg  адреса конфигурационных регистров
@{*/
// Адреса модулей для канала B отличается старшим разрядом
#define CH_A					0x00
#define CH_B					0x80

//Формирователь частоты 
#define ADDR_FACC				0x00

//Формирователь фазы 
#define ADDR_PHACC				0x10

//Память данных 
#define ADDR_DATA_RAM			0x20

//Делитель частоты
#define ADDR_FREQ_DIV			0x24

//Генератор шума
#define ADDR_NOIS_GEN			0x28

//Коэффициент усиления
#define ADDR_GAIN				0x30

//Смещение
#define ADDR_OFFSET				0x31

//Управление мультиплексором
#define ADDR_MUX				0x32

//Амплитудный модулятор
#define ADDR_AM					0x34

//NCO
#define ADDR_NCO				0x50

//Регистр управления светодиодом
#define ADDR_LED_CONTROL		0xFF

//Запрос версии прошивки
#define ADDR_VERSION_REQUEST	0xFE
//!@}



typedef enum LESO5_STATUS_t
{
	LESO5_OK = 0,			///! Все хорошо
	LESO5_ERROR = -1		///! Произошла ошибка
} LESO5_STATUS;

/*!
	Обновляет список устройств.
	\param[in]	maxPortNum максимальный номер COM порта для сканирования.
	\return		количество подключенных к компьютеру устройств LESO5.
*/
int LESO_DLL leso5UpdateDeviceList(int maxPortNum);

/*!
	Получение имени порта, по номеру устройства в списке. 
	\param[in]	devNum номер устройства в списке (нумеруются с нуля).
	\param[out]	portName указатель на контейнер(массив) для имени порта.
	\param[in]	portNameLen длина имени порта (длина передаваемого массива).
	\return		статус операции
*/
LESO5_STATUS LESO_DLL leso5GetPortDevice(int devNum, char *portName, unsigned int portNameLen);

/*!
	Открытие устройства.
	\param[in]	serialPort имя последовательного порта для открытия.
	\return		статус операции.
*/
LESO5_STATUS LESO_DLL leso5Open(const char *serialPort);

/*!
	Возвращает дескриптор и версию прошивки устройства.
	\param[out] descriptor указатель на контейнер(массив) для имени порта.
	\param[in]	descriptorLen длина масива под дескриптор.
	\return 	статус операции.
*/
LESO5_STATUS LESO_DLL leso5GetDeviceDescriptor(char *descriptor, unsigned int descriptorLen);

/*!
	Возвращает версию библиотеки.
	\detail
	4-х байтовый номер версии содержит страрший номер(major), младший(minor) и номер сборки(build version).
	Байт0 (младший значащий) содержит номер сборки, байт1 -- minor, байт2 -- major.
	Байт3 пока содержит ноль.
	Например,версия драйвера 1.02.14 будет представлена как 0x0001020E.
	\param[out] descriptor указатель на контейнер(массив) для имени порта.
	\param[in]	descriptorLen длина масива под дескриптор.
	\return 	статус операции.
*/
LESO5_STATUS LESO_DLL leso5GetDriverVersion(uint32_t *driverVersion);

/*!
	Записать значение в 16-ти битный регистр.
	\param[in]	regAddr адрес регистра.
	\param[in]	value значение регистра.
	\return		статус операции.
*/
LESO5_STATUS LESO_DLL leso5SetReg16(uint8_t regAddr, uint16_t value);

/*!
	Записать значение в 32-ти битный регистр.
	\param[in]	regAddr адрес регистра.
	\param[in]	value значение регистра.
	\return		статус операции.
*/
LESO5_STATUS LESO_DLL leso5SetReg32(uint8_t regAddr, uint32_t value);

/*!
	Закрыть устройство.
*/
void LESO_DLL leso5Close();

#ifdef __cplusplus
}
#endif

#endif
