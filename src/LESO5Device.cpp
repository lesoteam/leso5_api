
#include "LESO5Device.h"

LESO5Device::LESO5Device()
{
	
}

LESO5Device::~LESO5Device()
{
	switchLed(false);
}

int LESO5Device::open(const char *serialPort)
{
	int ret = -1;
	
	if (serialPort != NULL) {
		if (serial.Open(serialPort, DEVICE_BAUD_RATE) == 1) {
			if (isLESO5Device()) {
				ret = 0;
				switchLed(true);
			}
		}
		else
			ret = -1;
	}
	
	return ret;
}

void LESO5Device::close()
{
	switchLed(false);
	currentOpenDeviceName.clear();
	serial.Close();
}

void LESO5Device::deviceScan(int portNum)
{
	char portName[MAX_PORT_NAME_LENGTH];

	leso5DeviceLst.clear();
#ifdef _WINDOWS
	for (int i(1); i <= portNum; i++)
	{
		_snprintf_s(portName, MAX_PORT_NAME_LENGTH, COM_PORTS_NAME, i);
#else
	for(int i(0); i < portNum; i++)
	{
		snprintf(portName, MAX_PORT_NAME_LENGTH, COM_PORTS_NAME, i);
#endif
		if (serial.Open(portName, DEVICE_BAUD_RATE) == 1)
		{
			if (isLESO5Device()) {
				leso5DeviceLst.push_back(string(portName));
			}
			currentOpenDeviceName.clear();	
			serial.Close();
		}
	}
}

bool LESO5Device::isLESO5Device()
{
	char cmd[3] = {(char)0xFE, (char)0, (char)1};
	serial.Write(&cmd[0], 3);
	
	char deviceName[MAX_DEVICE_NAME_LENGTH];
	serial.ReadString(&deviceName[0], '\n', MAX_DEVICE_NAME_LENGTH, READ_TIMEOUT_MS);
	
	if (strncmp("LESO5", deviceName, 5) == 0) {
		currentOpenDeviceName = string(deviceName);
		return true;
	}
	
	return false;
}

void LESO5Device::switchLed(bool ledOn)
{
	char cmd[3];
	cmd[0] = (char)0xFF;
	cmd[1] = 0;
	if (ledOn)
		cmd[2] = 0x01;
	else
		cmd[2] = 0x00;
	
	serial.Write(&cmd[0], 3);
}

bool LESO5Device::sendCmd(char *cmd, int cmd_length)
{
	if(serial.Write(&cmd[0], cmd_length))
		return true;
	return false;
}
