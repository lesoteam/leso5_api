
#include "leso5.h"

#include "LESO5Device.h"

#include <cstdio>
#include <list>
#include <string>

using std::list;
using std::string;

LESO5Device dev;

#define MAX_DESCRIPTOR_SIZE 20

int leso5UpdateDeviceList(int maxPortNum)
{
	dev.deviceScan(maxPortNum);
	return dev.getDeviceLst()->size();
}

LESO5_STATUS leso5Open(const char *serialPort)
{
	return (LESO5_STATUS)dev.open(serialPort);
}

LESO5_STATUS leso5GetDeviceDescriptor(char *descriptor, unsigned int descriptorLen)
{
	if (descriptorLen < dev.getDeviceName().size())
		return LESO5_ERROR;
	else
		strcpy(descriptor, dev.getDeviceName().data());

	return LESO5_OK;
}

LESO5_STATUS leso5GetDriverVersion(uint32_t *driverVersion)
{
	*driverVersion =	((uint32_t)VERSION_MAJOR << 16) |
						((uint32_t)VERSION_MINOR << 8) |
						VERSION_BUILD;
	return LESO5_OK;
}

LESO5_STATUS leso5GetPortDevice(int devNum, char* portName, unsigned int portNameLen)
{
	int devicesNum = dev.getDeviceLst()->size();
	if (devNum > devicesNum - 1)
		return LESO5_ERROR;
	else if(portNameLen < dev.getDeviceLst()->at(devNum).size())
		return LESO5_ERROR;
	else 
		strcpy(portName, dev.getDeviceLst()->at(devNum).data());

	return LESO5_OK;
}



LESO5_STATUS leso5SetReg16(uint8_t regAddr, uint16_t value)
{
	char cmdLine[3];
	cmdLine[0] = (char)(regAddr & 0xFF);
	cmdLine[1] = (char)((value >> 8) & 0xFF);
	cmdLine[2] = (char)(value & 0xFF);  
	if(dev.sendCmd(&cmdLine[0], 3))
		return LESO5_OK;
	
	return LESO5_ERROR;
}

LESO5_STATUS leso5SetReg32(uint8_t regAddr, uint32_t value)
{
	LESO5_STATUS ret = LESO5_ERROR;
	
	if (leso5SetReg16(regAddr+1, value>>16) == LESO5_ERROR)
		return LESO5_ERROR;
	ret = leso5SetReg16(regAddr, value & 0xFFFF);
	return (ret);
}

void leso5Close()
{
	dev.close();
}
