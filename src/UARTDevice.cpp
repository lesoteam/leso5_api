
#include "UARTDevice.h"

UARTDevice::UARTDevice()
{
	openFlag = true;
}

UARTDevice::~UARTDevice()
{
	if(isOpen())
		serialib::Close();
}

char UARTDevice::Open(const char *Device, const unsigned int Bauds)
{
	char ret = serialib::Open(Device, Bauds);
	
	if(ret == 1)
		openFlag = true;
	else
		openFlag = false;
	
	return ret;
}