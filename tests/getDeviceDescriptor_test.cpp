#include "LESO5Device.h"

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <iostream>
#include <vector>

#define COM_PORT_NAME "COM3"
#define BYTE_NUM 3
#define DEVICE_NAME_SIZE 5

#define LED_ON 1

using std::cout;
using std::vector;

TEST_CASE("try read device descriptor", "[uart.open && uart.write && uart.read]") {
	LESO5Device uart;
	uart.deviceScan();
	cout << " ================ Find device list ================== \n";
	for (int i(0); i < uart.getDeviceLst()->size(); i++) { 
		cout << uart.getDeviceLst()->at(i) << "\n";
		uart.open(uart.getDeviceLst()->at(i).data());
		cout << uart.getDeviceName() << "\n";
		uart.close();
	}
	
	cout << "\n";
	
	uart.close();
}