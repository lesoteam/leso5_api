#include "UARTDevice.h"

#define CATCH_CONFIG_MAIN
#include <catch.hpp>

TEST_CASE("try Open ports", "[uart.open]") {
	UARTDevice uart;
	SECTION("Open com1"){
		REQUIRE(uart.Open("COM1", 9600) == 1);
		uart.Close();
	}
	
	SECTION("Open com2"){
		REQUIRE(uart.Open("COM2", 9600) == -1);
		uart.Close();
	}
	
	SECTION("Open com3"){
		REQUIRE(uart.Open("COM3", 9600) == 1);
		uart.Close();
	}
}
