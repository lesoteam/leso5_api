#include "leso5.h"

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <iostream>
#include <vector>

using std::cout;
using std::vector;

TEST_CASE("try read device descriptor", "[uart.open && uart.write && uart.read]") {
	char portName[20];
	char deviceName[20];
	REQUIRE(leso5Open("COM1") == LESO5_ERROR);
	REQUIRE(leso5GetPortDevice(0, &portName[0], 20) == LESO5_ERROR);
	cout << "============ Scan devices ==============\n";
	cout << "Found device num: " << leso5UpdateDeviceList(10) << "\n";
	if (leso5GetPortDevice(0, &portName[0], 20) != LESO5_ERROR) {
		cout << "Device access port: " << portName << "\n";
		REQUIRE(leso5Open(&portName[0]) == LESO5_OK);
		leso5GetDeviceDescriptor(&deviceName[0], 20);
		cout << deviceName << "\n";
	}
}